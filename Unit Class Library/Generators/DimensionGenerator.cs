﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitClassLibrary
{
    public static class DimensionGenerator
    {
        public static Distance MakeDimensionWithInches(double passedValue)
        {
            return new Distance(DimensionType.Inch, passedValue);
        }

        public static Distance MakeDimensionWithMillimeters(double passedValue)
        {
            return new Distance(DimensionType.Millimeter, passedValue);
        }
    }
}