﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitClassLibrary;

namespace UnitClassLibrary
{
    /// <summary>
    /// This class is to provide for two needs.
    ///   
    ///    1. Some complex units are not so easily described as Area or Speed  (explicit implementations).
    /// So another Construct needed to be available for describing things like  Meters^8/ Seconds ^3
    /// 
    ///    2. There are times when the Units by which a value is described need to be represented using the same type. 
    /// For example, Sometimes a vector needs to be represented with a Magnitude that is a force, other times it is a dimension. 
    /// If the Distance Vector inherits its Magnitude from its parent e.g. LineSegment's length, Magnitude will be a Distance. 
    /// Whereas it is reasonable that a Force Vector could be desired. 
    /// 
    /// </summary>
    public class Unit: IDistance
    {

        // base unit powers. For example Area: (M=0, L=2, T=0)
        // -1 means inverted 
        // fractions mean root

        readonly int MassExponent;
        readonly int DistanceExponent;
        readonly int TimeExponent;
        readonly int ElectricCurrentExponent;
        readonly int TemperatureExponent;
        readonly int QuantityExponent;
        readonly int LuminousIntensityExponent;

        readonly int AngleExponent;

        UnitType UnitType;

        DimensionType BaseDimensionUnitType = DimensionType.Millimeter;
        MassType BaseMassUnitType = MassType.Gram;
        //...




        readonly double _intrinsicValue; // base unit value.




        public Unit(IDistance passedDistance)
        {
            this._intrinsicValue = passedDistance.GetValue(BaseDimensionUnitType);
        }

        public Unit(IMass passedMass)
        {
            this._intrinsicValue = passedMass.Grams;
        }

        public Unit(IArea passedArea)
        {
            this._intrinsicValue = passedArea.MillimetersSquared;
        }

        public Unit(Unit other)
        {
            this.MassExponent = other.MassExponent;
            this.DistanceExponent = other.DistanceExponent;
            this.TimeExponent = other.TimeExponent;
            this._intrinsicValue = other._intrinsicValue;
        }


        public bool IsConvertibleTo(Unit other) { throw new NotImplementedException(); }
        public double FactorTo(Unit target)
        {
            if (IsConvertibleTo(target))
            {
                return _intrinsicValue / target._intrinsicValue;
            }
            throw new ArgumentException("Incompatible units in target.");
        }
        public override string ToString()
        {
            return string.Format("{0}(M:{1},L:{2},T:{3})", _intrinsicValue, MassExponent, DistanceExponent, TimeExponent);
        }
        public static Unit operator *(double relative, Unit unit)
        {
            return new Unit(unit.MassExponent, unit.DistanceExponent, unit.TimeExponent, relative * unit._intrinsicValue);
        }
        public static Unit operator /(Unit unit, double divisor)
        {
            return new Unit(unit.MassExponent, unit.DistanceExponent, unit.TimeExponent, unit._intrinsicValue / divisor);
        }

        public static Unit operator *(Unit unit, Unit other)
        {
            return new Unit(
                unit.MassExponent + other.MassExponent,
                unit.DistanceExponent + other.DistanceExponent,
                unit.TimeExponent + other.TimeExponent,
                unit._intrinsicValue * other._intrinsicValue);
        }
        public static Unit operator /(Unit unit, Unit other)
        {
            return new Unit(
                unit.MassExponent - other.MassExponent,
                unit.DistanceExponent - other.DistanceExponent,
                unit.TimeExponent - other.TimeExponent,
                unit._intrinsicValue / other._intrinsicValue);
        }

        public static Unit operator ^(Unit unit, int power)
        {
            return new Unit(
                unit.MassExponent * power,
                unit.DistanceExponent * power,
                unit.TimeExponent * power,
                Math.Pow(unit._intrinsicValue, power));
        }

        private bool IsUnitType(UnitType passedUnitType)
        {
            return passedUnitType == this.UnitType;

        }


        public double Centimeters
        {
            get 
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Centimeters;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }
                
            }
        }

        public double Feet
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Feet;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double Inches
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Inches;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double Kilometers
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Kilometers;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        double Meters
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Meters;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double Miles
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Miles;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double Millimeters
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Millimeters;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double Sixteenths
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Sixteenths;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double ThirtySeconds
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).ThirtySeconds;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }

        public double Yards
        {
            get
            {
                if (IsUnitType(UnitType.Distance))
                {
                    return new Distance(BaseDimensionUnitType, _intrinsicValue).Yards;
                }
                else
                {
                    throw new Exception("This Unit object does not represent Distance");
                }

            }
        }
    }
}
