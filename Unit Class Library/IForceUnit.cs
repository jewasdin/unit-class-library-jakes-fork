﻿using System;
namespace UnitClassLibrary
{
    interface IForceUnit
    {
        double Kips { get; }
        double Newtons { get; }
        double Pounds { get; }
    }
}
