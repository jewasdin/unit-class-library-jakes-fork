﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitClassLibrary
{
    public interface IMass
    {
        double Grams{ get; }

        double Kilograms{ get; }

    }
}
