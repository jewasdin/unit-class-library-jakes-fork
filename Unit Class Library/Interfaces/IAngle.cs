﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitClassLibrary
{
    public interface IAngle
    {
        double Radians{ get; }

        double Degrees{ get; }

        double GetValue(AngleType passedAngle);

    }
}
