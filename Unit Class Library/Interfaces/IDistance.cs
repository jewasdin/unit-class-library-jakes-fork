﻿using System;
namespace UnitClassLibrary
{
    public interface IDistance
    {
        double Centimeters { get; }
        double Feet { get; }
        double Inches { get; }
        double Kilometers { get; }
        double Meters { get; }
        double Miles { get; }
        double Millimeters { get; }
        double Sixteenths { get; }
        double ThirtySeconds { get; }
        double Yards { get; }

        double GetValue(DimensionType passedAngle);
    }
}
